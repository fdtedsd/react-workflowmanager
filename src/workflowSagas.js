import { spawn, call, put, takeEvery } from 'redux-saga/effects';
import { getProcess, getActivityManager } from './workflowService';
import { FETCH_PROCESS, SAVE_PROCESS, ERROR_FETCH_PROCESS, FETCH_ACTIVITY_MANAGER, SAVE_ACTIVITY_MANAGER, ERROR_FETCH_ACTIVITY_MANAGER } from './workflowActionTypes';

export const EffectTypes = {
    activityManager: 'activityManager',
};

export const EffectActions = {
    save: 'save',
    error: 'error',
};

function* fetchProcess(action) {
    const response = yield call(getProcess, action.payload);

    if (response && response.process) {
        yield put({ type: SAVE_PROCESS, payload: response.process });
    } else {
        yield put({ type: ERROR_FETCH_PROCESS, payload: { response: response, processId: action.payload } });
    }
}

function* watchFetchProcess() {
    yield takeEvery(FETCH_PROCESS, fetchProcess);
}

function* fetchActivityManager(action) {
    const response = yield call(getActivityManager, action.payload.activityManagerId, action.payload.processId);
    if (response && response.activityManager) {
        yield put({ type: SAVE_ACTIVITY_MANAGER, payload: response.activityManager });
    } else {
        yield put({
            type: ERROR_FETCH_ACTIVITY_MANAGER,
            payload: {
                repsonse: response,
                processId: action.payload.processId,
                activityManagerId: action.payload.activityManagerId,
            }
        })
    }
}

function* watchFetchActivityManager() {
    yield takeEvery(FETCH_ACTIVITY_MANAGER, fetchActivityManager);
}

const saveActivityManagerEffects = [
    {
        type: EffectTypes.activityManager,
        action: EffectActions.save,
        status: 'started',
    }
];
function* triggerSaveActivityManagerEffect(action) {
    const payload = action.payload;
    for (const effect of saveActivityManagerEffects) {
        if (payload.activity_status === effect.status) {
            let actionType = `WF/${payload.props.action}`;
            if (effect.status !== 'started') {
                actionType = actionType + '/' + effect.status;
            }
            yield put({ type: actionType, payload: { id: payload.id, status: payload.activity_status, ...payload.props, processId: payload.process_id, activities: payload.activities } });
        }
    }
}

function* watchSaveActivityManager() {
    yield takeEvery(SAVE_ACTIVITY_MANAGER, triggerSaveActivityManagerEffect);
}

const errorActivityManagerEffects = [];
function* triggerErrorActivityManagerEffect(action) {
    const payload = action.payload;
    for (const effect of errorActivityManagerEffects) {
        yield put({ type: effect.actionType, payload: { ...payload } });
    }
}

function* watchErrorActivityManager() {
    yield takeEvery(ERROR_FETCH_ACTIVITY_MANAGER, triggerErrorActivityManagerEffect);
}

export function setup(sideEffects) {
    for (const effect of sideEffects) {
        if (effect.type === EffectTypes.activityManager) {
            if (effect.action === EffectActions.save) {
                saveActivityManagerEffects.push(effect);
            } else if (effect.action === EffectActions.error) {
                errorActivityManagerEffects.push(effect);
            }
        }
    }
}

function* init() {
    yield spawn(watchFetchProcess);
    yield spawn(watchFetchActivityManager);
    yield spawn(watchSaveActivityManager);
    yield spawn(watchErrorActivityManager);
}

export default init;