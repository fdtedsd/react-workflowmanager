let configGetToken;
let configGetProcess;
let configGetActivityManager;
let configGetActivityManagerForProcess;

export function setup({ getToken, getProcess, getActivityManager, getActivityManagerForProcess }) {
    configGetToken = getToken;
    configGetProcess = getProcess;
    configGetActivityManager = getActivityManager;
    configGetActivityManagerForProcess = getActivityManagerForProcess;
}

export async function getProcess(processId) {
    if (typeof configGetProcess === 'string') {
        const token = await configGetToken();
        let headers;
        if (token) {
            headers = {
                Authorization: `Bearer ${token}`,
            }
        }
        const response = await fetch(configGetProcess.replace(':processId', processId), {
            headers: headers,
        });

        let responseProcess;
        if (response.ok) {
            responseProcess = await response.json();
        }

        return {
            status: response.status,
            process: responseProcess,
        };
    } else if (typeof configGetProcess === 'function') {
        return await configGetProcess(processId);
    }
}

export async function getActivityManager(activityManagerId, processId) {
    if (typeof configGetActivityManager === 'string') {
        const token = await configGetToken();
        let headers;
        if (token) {
            headers = {
                Authorization: `Bearer ${token}`,
            }
        }
        const response = await fetch(configGetActivityManager.replace(':activityManagerId', activityManagerId), {
            headers: headers,
        });

        let responseActivityManager;
        if (response.ok) {
            responseActivityManager = await response.json();
        }

        return {
            status: response.status,
            activityManager: responseActivityManager,
        };
    } else if (typeof configGetActivityManager === 'function') {
        return await configGetActivityManager(activityManagerId, processId);
    }
}

export async function getActivityManagerForProcess(processId) {
    if (typeof configGetActivityManager === 'string') {
        const token = await configGetToken();
        let headers;
        if (token) {
            headers = {
                Authorization: `Bearer ${token}`,
            }
        }
        const response = await fetch(configGetActivityManagerForProcess.replace(':processId', processId), {
            headers: headers,
        });

        let responseActivityManager;
        if (response.ok) {
            responseActivityManager = await response.json();
        }

        return {
            status: response.status,
            activityManager: responseActivityManager,
        };
    } else if (typeof configGetActivityManager === 'function') {
        return await configGetActivityManager(processId);
    }
}