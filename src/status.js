export const connectionStatus = {
    CONNECTED: 'CONNECTED',
    DISCONNECTED: 'DISCONNECTED',
}

export const dataLoadStatus = {
    LOADING: 'LOADING',
    FINISHED: 'FINISHED',
    ERROR: 'ERROR',
}