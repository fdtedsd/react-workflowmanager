import mqtt from 'mqtt';

let client;

function connect(connection) {
    client = mqtt.connect(connection);
}

function disconnect() {
    return new Promise((resolve, reject) => {
        function disconect() {
            client = undefined;
            resolve();
        }
        if (client) {
            client.end(false, {}, disconect);
        } else {
            resolve();
        }
    })
}

function listen(event, callback) {
    let listening = false;
    if (client) {
        client.on(event, callback);
        listening = true;
    }

    return listening;
}

function addMessageListener(callback) {
    let listening = false;
    if (client) {
        client.on('message', (topic, payload, packet) => callback(topic, payload.toString(), packet));
        listening = true;
    }
    return listening;
}

function removeMessageListener(listener) {
    if (client) {
        client.removeListener('message', listener);
    }
}

function subscribe(topic) {
    return new Promise((resolve, reject) => {
        if (client) {
            client.subscribe(topic, {}, (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        } else {
            reject('Not connected');
        }
    });
}

function unsubscribe(topic) {
    return new Promise((resolve, reject) => {
        if (client) {
            client.unsubscribe(topic, {}, (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            })
        } else {
            reject('Not connected');
        }
    })
}

export default {
    get client(){
        return client
    },
    connect,
    disconnect,
    subscribe,
    unsubscribe,
    listen,
    addMessageListener,
    removeMessageListener,
};
