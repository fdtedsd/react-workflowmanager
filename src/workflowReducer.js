import { SAVE_PROCESS, SAVE_ACTIVITY_MANAGER, CHANGE_CONNECTION_STATUS, CHANGE_DATA_LOAD_STATUS } from "./workflowActionTypes";
import { connectionStatus, dataLoadStatus } from "./status";

const initialState = {
    processes: {},
    activityManagers: [],
    connectionStatus: connectionStatus.DISCONNECTED,
    dataLoadStatus: null,
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case SAVE_PROCESS: {
            const resultState = {
                ...state,
            };
            resultState.processes[action.payload._id] = action.payload;
            return resultState;
        }
        case SAVE_ACTIVITY_MANAGER: {
            const resultState = {
                ...state,
            };
            let index = 0;
            if (resultState.activityManagers.length > 0) {
                let newCreationDate = new Date(action.payload.created_at);
                let currentCreationDate = new Date(resultState.activityManagers[index].created_at);
                while (currentCreationDate > newCreationDate && index < resultState.activityManagers.length) {
                    index += 1;
                    if (index < resultState.activityManagers.length) {
                        currentCreationDate = new Date(resultState.activityManagers[index].created_at);
                    }
                }
            }
            resultState.activityManagers.splice(index, 0, action.payload);
            return resultState;
        }
        case CHANGE_CONNECTION_STATUS: {
            return {
                ...state,
                connectionStatus: action.payload,
            };
        }
        case CHANGE_DATA_LOAD_STATUS: {
            return {
                ...state,
                dataLoadStatus: action.payload,
            };
        }
        default:
            return state;
    }
}