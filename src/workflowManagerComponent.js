import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import mqttClient from './mqttClient';
import { fetchProcess, fetchActivityManager, saveProcess, saveActivityManager, changeConnectionStatus, changeDataLoadStatus } from './workflowActions';
import { getActivityManagerForProcess, getProcess } from './workflowService';
import { connectionStatus, dataLoadStatus } from './status';

class WorkflowManager extends React.Component {
    constructor(props) {
        super(props);
        this.messageListener = (...args) => this.onMessageArrived(...args);
        mqttClient.addMessageListener(this.messageListener);
    }

    async onMessageArrived(topic, message, packet) {
        try {
            if (/\/notify\/process\/.+\/activity_manager\/.+$/.test(topic)) {
                const messageData = JSON.parse(message);
                if (messageData.processId) {
                    this.props.fetchProcess(messageData.processId);
                }
                if (messageData.activityManagerId) {
                    this.props.fetchActivityManager({
                        processId: messageData.processId,
                        activityManagerId: messageData.activityManagerId,
                    });
                }
            }
        } catch (error) {
            console.error(error);
        }
    }

    async loadProcessData(processId) {
        const processPromise = getProcess(processId);
        const activityManagerPromise = getActivityManagerForProcess(processId);

        const processResult = await processPromise;
        const activityManagerResult = await activityManagerPromise;

        this.props.saveProcess(processResult.process);
        this.props.saveActivityManager(activityManagerResult.activityManager);
    }

    componentDidMount() {
        mqttClient.listen('connect', () => {
            this.props.changeConnectionStatus(connectionStatus.CONNECTED);
        });
        mqttClient.listen('offline', () => {
            this.props.changeConnectionStatus(connectionStatus.DISCONNECTED);
        });
        if (location && location.search) {
            const urlSearch = new URLSearchParams(location.search);
            const processParameter = urlSearch.get('p');
            if (processParameter) {
                this.props.changeDataLoadStatus(dataLoadStatus.LOADING);
                const processIds = urlSearch.get('p').split(';');
                const promises = [];

                for (const processId of processIds) {
                    promises.push(this.loadProcessData(processId));
                }
                Promise.all(promises).then(() => {
                    this.props.changeDataLoadStatus(dataLoadStatus.FINISHED);
                }, () => {
                    this.props.changeDataLoadStatus(dataLoadStatus.ERROR);
                })
            } else {
                this.props.changeDataLoadStatus(dataLoadStatus.FINISHED);
            }
        } else {
            this.props.changeDataLoadStatus(dataLoadStatus.FINISHED);
        }
    }

    componentWillUnmount() {
        mqttClient.removeMessageListener(this.messageListener);
    }

    render() {
        let element = null;
        if (this.props.connectionStatus === connectionStatus.CONNECTED
            && this.props.dataLoadStatus === dataLoadStatus.FINISHED) {
            element = this.props.children;
        } else if (this.props.dataLoadStatus === dataLoadStatus.ERROR) {
            element = <div>Error loading data</div>
        }
        return element;
    }
}

function mapStateToProps(state) {
    return {
        connectionStatus: state.workflow.connectionStatus,
        dataLoadStatus: state.workflow.dataLoadStatus,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchProcess: bindActionCreators(fetchProcess, dispatch),
        saveProcess: bindActionCreators(saveProcess, dispatch),
        fetchActivityManager: bindActionCreators(fetchActivityManager, dispatch),
        saveActivityManager: bindActionCreators(saveActivityManager, dispatch),
        changeConnectionStatus: bindActionCreators(changeConnectionStatus, dispatch),
        changeDataLoadStatus: bindActionCreators(changeDataLoadStatus, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkflowManager);