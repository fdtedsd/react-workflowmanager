export function groupByProcess(activityManagers) {
    return activityManagers.reduce((accumulator, value) => {
        const listForProcess = accumulator[value.process_id] || [];
        listForProcess.push(value);
        accumulator[value.process_id] = listForProcess;
        return accumulator;
    }, {});
}