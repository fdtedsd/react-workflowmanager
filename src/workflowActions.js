import { FETCH_PROCESS, FETCH_ACTIVITY_MANAGER, SAVE_PROCESS, SAVE_ACTIVITY_MANAGER, CHANGE_CONNECTION_STATUS, CHANGE_DATA_LOAD_STATUS } from "./workflowActionTypes";

export function fetchProcess(processId) {
    return {
        type: FETCH_PROCESS,
        payload: processId,
    };
}

export function saveProcess(process) {
    return {
        type: SAVE_PROCESS,
        payload: process,
    };
}

export function fetchActivityManager({processId, activityManagerId}) {
    return {
        type: FETCH_ACTIVITY_MANAGER,
        payload: {processId, activityManagerId},
    };
}

export function saveActivityManager(activityManager){
    return {
        type: SAVE_ACTIVITY_MANAGER,
        payload: activityManager,
    };
}

export function changeConnectionStatus(status) {
    return {
        type: CHANGE_CONNECTION_STATUS,
        payload: status,
    };
}

export function changeDataLoadStatus(status) {
    return {
        type: CHANGE_DATA_LOAD_STATUS,
        payload: status,
    };
}