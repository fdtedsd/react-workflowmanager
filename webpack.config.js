const path = require('path');

module.exports = {
	entry: './src/index.js',
	mode: 'development',
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|dist)/,
				loader: 'babel-loader',
				options: {presets: ['@babel/env']}
			}
		]
	},
	output: {
		path: path.resolve(__dirname, 'dist/'),
		// publicPath: '/dist/',
		filename: 'index.js',
		libraryTarget: 'commonjs2',
	},
	externals: {
		react: 'react',
		'react-dom': 'react-dom',
		'react-redux': 'react-redux',
		redux: 'redux',
		'redux-saga': 'redux-saga',
		'redux-saga/effects': 'redux-saga/effects',
	},
	// devServer: {
	// 	contentBase: path.join(__dirname, 'public/'),
	// 	port: 3000,
	// 	publicPath: 'http://localhost:3000/dist/',
	// 	hotOnly: true,
	// },
};
